
import java.util.Scanner;

public class CommandPanel {

    CoffeeMachine coffeeMachine;

    public CommandPanel(CoffeeMachine coffeeMachine) {
        this.coffeeMachine = coffeeMachine;
    }

    public void ChooseCoffee() {
        while (true) {
            System.out.println("Please Choose:");
            System.out.println("1. Espresso");
            System.out.println("2. House Blend");
            System.out.println("3. List of sold coffees");
            System.out.println("");

            Scanner scanner = new Scanner(System.in);

            try {

                int a = scanner.nextInt();

                switch (a) {
                    case 1:
                        coffeeMachine.ChooseBeverageType("espresso");
                        System.out.println("");
                        ChooseIngredient();
                        break;
                    case 2:
                        coffeeMachine.ChooseBeverageType("house");
                        System.out.println("");
                        ChooseIngredient();
                        break;
                    case 3:
                        coffeeMachine.GetListOfBeverages();
                        System.out.println("");
                        break;
                }

            } catch (Exception ex) {
                System.out.println("Something went wrong. Please try again.");
            }

        }
    }

    public void ChooseIngredient() {
        
        Boolean choosing = true;
        while (choosing) {
            System.out.println("Please Choose ingredients:");
            System.out.println("1. Milk");
            System.out.println("2. Mocha");
            System.out.println("3. Whip");
            System.out.println("4. Finish");
            System.out.println("");

            Scanner scanner = new Scanner(System.in);

            try {

                int a = scanner.nextInt();

                switch (a) {
                    case 1:
                        coffeeMachine.ChooseCondiment("milk");
                        System.out.println("");
                        break;
                    case 2:
                        coffeeMachine.ChooseCondiment("mocha");
                        System.out.println("");
                        break;
                    case 3:
                        coffeeMachine.ChooseCondiment("whip");
                        System.out.println("");
                        break;
                    case 4:
                        coffeeMachine.Finish();
                        choosing = false;
                        break;
                }

            } catch (Exception ex) {
                System.out.println("Something went wrong. Please try again.");
            }

        }
    }

}
