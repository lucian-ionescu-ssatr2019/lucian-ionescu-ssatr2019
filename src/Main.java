
import java.util.Scanner;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        CoffeeMachine coffeeMachine = new CoffeeMachine();
        
        CommandPanel commandPanel = new CommandPanel(coffeeMachine);
        
        commandPanel.ChooseCoffee();
    
    }

}
