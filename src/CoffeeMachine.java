
import java.util.ArrayList;

public class CoffeeMachine {
    Beverage beverage;
    ArrayList<Beverage> beverages;
    
    public CoffeeMachine(){
        beverages = new ArrayList();
    }
    
    public void ChooseBeverageType(String type){
        if (type.equals("espresso")){
            beverage = new Espresso();
        }
        if (type.equals("house")){
            beverage = new HouseBlend();
        }
        System.out.print("Coffe description: "+ beverage.getDescription()+" | ");
        System.out.println("Price: "+ beverage.cost()+" $");
    }
    
    public void ChooseCondiment(String type){
        if (type.equals("milk")){
            beverage = new Milk(beverage);
        }
        if (type.equals("mocha")){
            beverage = new Mocha(beverage);
        }
        if (type.equals("whip")){
            beverage = new Whip(beverage);
        }
        System.out.print("Coffe description: "+ beverage.getDescription()+" | ");
        System.out.println("Price: "+ beverage.cost()+" $");
    }
    
    public void Finish(){
        beverages.add(beverage);
    }
    
    public void GetListOfBeverages(){
        if (beverages != null){
            System.out.println("Coffes list:");
            for (int i = 0; i < beverages.size();i++){
                System.out.println( i+1+". "+ beverages.get(i).getDescription() +", Cost: "+beverages.get(i).cost());
            }
        }
        else{
            System.out.println("No coffes sold.");
        }
    }
}
